# frozen_string_literal: true
require_relative '../exercises/bucket_container'

describe BucketContainer do
  subject(:bucket_container) { described_class.new }

  describe '#add!' do
    it { expect(bucket_container.add!('key', 'val')).to eq('val') }
    it { expect(bucket_container.add!(:key, :val)).to eq(:val) }
    it { expect(bucket_container.add!(1, 2)).to eq(2) }
  end

  describe '#find_val' do
    before do
      [[1, 2], ['key', 'val'], [:key, :val]].each { |pair| bucket_container.add!(*pair) }
    end

    it { expect(bucket_container.find_val('key')).to eq('val') }
    it { expect(bucket_container.find_val(:key)).to eq(:val) }
    it { expect(bucket_container.find_val(1)).to eq(2) }
    it { expect(bucket_container.find_val(:nothing)).to be_nil }
  end

  describe '#replace_or_add!' do
    before do
      [[1, 2], ['key', 'val'], [:key, :val]].each { |pair| bucket_container.replace_or_add!(*pair) }
    end

    context 'add' do
      it { expect(bucket_container.size).to eq(3) }
      it { expect(bucket_container.find_val(1)).to eq(2) }
      it { expect(bucket_container.find_val('key')).to eq('val') }
      it { expect(bucket_container.find_val(:key)).to eq(:val) }
      it { expect(bucket_container.find_val(:nothing)).to be_nil }
    end

    context 'replace' do
      before do
        [[1, 3], ['key', 'val2'], [:key, :val2]].each { |pair| bucket_container.replace_or_add!(*pair) }
      end

      it { expect(bucket_container.size).to eq(3) }
      it { expect(bucket_container.find_val(1)).to eq(3) }
      it { expect(bucket_container.find_val('key')).to eq('val2') }
      it { expect(bucket_container.find_val(:key)).to eq(:val2) }
      it { expect(bucket_container.find_val(:nothing)).to be_nil }
    end
  end
end
