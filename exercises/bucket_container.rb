class BucketContainer < Array
  def add!(key, val)
    self.push [key, val]
    val
  end

  def find_val(key)
    self.find {|pair| pair[0] == key}&.fetch(1, nil)
  end

  def replace_or_add!(key, val)
    if i = self.find_index {|pair| pair[0] == key }
      self[i][1] = val
      false
    else
      self.push [key, val]
      true
    end
  end
end
