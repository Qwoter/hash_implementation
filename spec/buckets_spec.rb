# frozen_string_literal: true
require_relative '../exercises/buckets'

describe Buckets do
  subject(:buckets) { described_class.new }

  describe '#[]' do
    before do
      buckets[0] = [:key, :val]
    end

    it { expect(buckets[11]).to eq(BucketContainer.new) }
    it { expect(buckets[1]).to eq(BucketContainer.new) }
    it { expect(buckets[0]).to eq([:key, :val]) }
  end
end
