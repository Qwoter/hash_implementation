require_relative 'bucket_container'

class Buckets < Array
  def [](index)
    super(index) || self[index] = BucketContainer.new
  end
end
