Hash implementation
=======

## How to run

Test for the main class `HashTable`
```
rspec test/test.rb
```

Auxiliary tests for `Buckets` and `BucketContainer`
```
rspec spec
```

Benchmark
```
ruby benchmark.rb
```

Profiling
```
ruby profile.rb
```

## Decision explanation

Hashtable needs next things in order to function:

- Hash function
- Buckets size
- Buckets array
- Bucket container (usually linked list)
- Hash recalculation when changing bucket size

### Hash function

I chose to use default `.hash` as it is used in ruby implementation of Hash

### Buckets size

The best results are when:

- value is roughly doubled each time we reach a limit so we don't recalculate Hash to often
- value is prime so we have less collisions

### Buckets array

Regular array but with added functionality to have a default value of `BucketContainer`. Ruby array has it's own default value syntax but i didn't want to clatter the memory by initializing every value of Buckets from the start.

### Bucket container

Ideally should be a linked list or self balancing tree but i opted to go with 2d Array.
Additional methods help us to manipulate data inside 2d array and not clatter `Hashtable` implementation with unrelated methods.

### Hash recalculation when changing bucket size

Nothing fancy here i recalculate buckets using `add!` instead of `replace_or_add!` because there are no duplicates.

## Happy Hacking
Only 2 times slower on 50k elements than Ruby implementation :hear_no_evil: