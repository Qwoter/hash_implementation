require_relative 'buckets'

class HashTable
  MRI_PRIMES = [
    8 + 3, 16 + 3, 32 + 5, 64 + 3, 128 + 3, 256 + 27, 512 + 9, 1024 + 9, 2048 + 5, 4096 + 3,
    8192 + 27, 16384 + 43, 32768 + 3, 65536 + 45, 131072 + 29, 262144 + 3, 524288 + 21, 1048576 + 7,
    2097152 + 17, 4194304 + 15, 8388608 + 9, 16777216 + 43, 33554432 + 35, 67108864 + 15,
    134217728 + 29, 268435456 + 3, 536870912 + 11, 1073741824 + 85
  ].map
  attr_reader :size

  def initialize
    @bucket_count = MRI_PRIMES.next
    @buckets = Buckets.new
    @load_factor = 1
    @size = 0
  end

  def []=(key, val)
    added = @buckets[bucket_index(key)].replace_or_add!(key, val)
    @size += 1 if added
    check_load_factor
    val
  end

  def [](key)
    @buckets[bucket_index(key)].find_val(key)
  end

  private

  def quick_assignment(key, val)
    @buckets[bucket_index(key)].add!(key, val)
  end

  def bucket_index(key)
    key.hash % @bucket_count
  end

  def check_load_factor
    expand_hash if @size.fdiv(@bucket_count) > @load_factor
  end

  def expand_hash
    @bucket_count = MRI_PRIMES.next
    temp_buckets = @buckets
    @buckets = Buckets.new

    temp_buckets.flatten(1).each do |(key, val)|
      quick_assignment(key, val)
    end
  end
end