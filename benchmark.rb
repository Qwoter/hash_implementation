# frozen_string_literal: true
require_relative 'exercises/hash_table'
require 'benchmark'
require 'securerandom'

n = 50_000
h = HashTable.new
h1 = {}

Benchmark.bm do |benchmark|
  benchmark.report("HashTable") do
    n.times do
      random_str = SecureRandom.hex
      h[random_str] = random_str
      h[random_str]
    end
  end

  benchmark.report("{}") do
    n.times do
      random_str = SecureRandom.hex
      h1[random_str] = random_str
      h1[random_str]
    end
  end
end
