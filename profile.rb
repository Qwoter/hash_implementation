require_relative 'exercises/hash_table'
require 'ruby-prof'
require 'securerandom'

h = HashTable.new

result = RubyProf.profile do
  10_000.times do
    random_str = SecureRandom.hex
    h[random_str] = random_str
    h[random_str]
  end
end

printer = RubyProf::GraphPrinter.new(result)
printer.print(STDOUT, {})
